<?php
namespace PHPList\Classes;

use PHPList\Core\PHPListException;


/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 22/11/2016
 * Time: 16:22
 */
class PHPList
{
    private $masterArray = array();
    public static $test = "no";

    public function addItem($itemContents)
    {
        if(array_push($this->masterArray , $itemContents))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getItem($itemToGet)
    {
        return $itemToGet;
    }

    public function getAllItems()
    {
        if(empty($this->masterArray))
        {
            // we can now throw our new exception to the application
            throw new PHPListException("List is empty");
        }
        else
        {
            return $this->masterArray;
        }

    }
}

class FabException extends \Exception
{

}