<?php
namespace PHPList\Core;
/**
 * Created by PhpStorm.
 * User: idris
 * Date: 24/11/2016
 * Time: 02:23
 *
 * This interface ensure that the implementing classes have the
 * required methods for creating a custom exception.
 */
interface FabIException
{
    /* Protected methods inherited from Exception class */
    public function getMessage();                 // Exception message
    public function getCode();                    // User-defined Exception code
    public function getFile();                    // Source filename
    public function getLine();                    // Source line
    public function getTrace();                   // An array of the backtrace()
    public function getTraceAsString();           // Formated string of trace

    /* Overrideable methods inherited from Exception class */
    public function __toString();                 // formated string for display
    public function __construct($message = null, $code = 0);

}