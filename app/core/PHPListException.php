<?php
/**
 * Created by PhpStorm.
 * User: idris
 * Date: 24/11/2016
 * Time: 02:27
 */

namespace PHPList\Core;
use PHPList\Core\FabIException;

class PHPListException extends \Exception implements FabIException
{

    protected $message  = 'Perhaps, list it is empty';           // Exception message
    private   $string;                                           // Unknown
    protected $code    = 007;                                    // User-defined exception code
    protected $file;                                             // Source filename of exception
    protected $line;                                             // Source line of exception
    private   $trace;                                            // Unknown

    public function __construct($message = null, $code = 0)
    {
        if (!$message) {
            throw new $this('Unknown '. get_class($this));
        }
        parent::__construct($message, $code);
    }

    public function __toString()
    {
        return get_class($this) . " '{$this->message}' in {$this->file}({$this->line})\n"
        . "{$this->getTraceAsString()}";
    }




}