<?php
use PHPUnit\Framework\TestCase;
use PHPList\Classes\PHPList;

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 22/11/2016
 * Time: 16:00
 */
class ListTest extends TestCase
{
    private static $myList;

    public static function setUpBeforeClass()
    {
        self::$myList = new PHPList();
    }

    public static function tearDownAfterClass()
    {
        self::$myList = null;
    }

    /**
     * @expectedException        \PHPList\Core\PHPListException
     * @expectedExceptionCode    007
     * @expectedExceptionMessage List is empty
     */
    public function testIfListIsEmpty()
    {
        $fabList = new PHPList();       // Here we are not using the static class, because we really want to check when
        $fabList->getAllItems();
    }

    public function testAddItemToList()
    {
        $returnBool = self::$myList->addItem("Luke");
        $this->assertTrue($returnBool);
    }

    public function testGetItemFromList()
    {
        $item = self::$myList->getItem("Luke");
        $this->assertEquals("Luke", $item);
    }

    public function testGetAllItemsFromList()
    {
        $allItems = self::$myList->getAllItems();
        $this->assertInternalType('array' , $allItems);
    }

}